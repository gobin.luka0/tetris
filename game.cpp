// Contains all game simulation logic

#define is_down(b) input->buttons[b].is_down
#define pressed(b) (is_down(b) && input->buttons[b].changed)
#define released(b) (!is_down(b) && input->buttons[b].changed)

const int arena_y = 25;
const int arena_x = 22;
int arena_half_size_x { 42 }, arena_half_size_y { 50 };
int shape_p_x { 0 }, shape_p_y { 48 };
float time_elapsed { 0 };
u32 arena_color { 0x666666 };
u32 arena[arena_y][arena_x];

void initializeArena() {
    for (int i = 0; i < arena_y; i++) {
        for (int j = 0; j < arena_x; j++) {
            arena[i][j] = arena_color;
        }
    }
}

const char* shapes[5] = {
    "00000"
    "00000"
    "00X00"
    "00000"
    "00000",

    "00000"
    "00X00"
    "00X00"
    "00XX0"
    "00000",

    "00000"
    "00X00"
    "00X00"
    "0XX00"
    "00000",

    "00X00"
    "00X00"
    "00X00"
    "00X00"
    "00X00",

    "00000"
    "00X00"
    "00X00"
    "00X00"
    "00000"
};

const u32 colors[6] = {
    0xFF6347, // Tomato
    0xFFD700, // Gold
    0xFF1493, // Deep Pink
    0x8A2BE2, // Blue Violet
    0xFFA500, // Orange
    0x00FF00  // Lime
};

struct Square {
    bool is_block;
    int p_x;
    int p_y;
};

struct Shape {
    bool is_falling;
    u32 color;
    Square squares[25];
};

Shape shape { is_falling: false };

internal int getRandomNumber(int limit) {
    return rand() % limit;
}

internal void populateSquares() {
    char chosen_shape[25];
    strcpy(chosen_shape, shapes[getRandomNumber(5)]);
    int counter = 0;
    for (int y = 64; y >= 48; y -= 4) {
        for(int x = -8; x <= 8; x += 4) {
            bool block;
            if (chosen_shape[counter] == 'X') block = true;
            else block = false;

            shape.squares[counter] = (Square){ .is_block = block, .p_x = x, .p_y = y };
            counter++;
        }
    }
}

internal bool touchingLeftWall() {
    for(int i = 0; i < 25; i++)
        if (shape.squares[i].p_x <= -arena_half_size_x + 2 && shape.squares[i].is_block) return true;

    return false;
}

internal bool touchingRightWall() {
    for(int i = 0; i < 25; i++)
        if (shape.squares[i].p_x >= arena_half_size_x - 2 && shape.squares[i].is_block) return true;

    return false;
}

internal bool touchingGround() {
    for(int i = 0; i < 25; i++)
        if (shape.squares[i].p_y <= -arena_half_size_y + 2 && shape.squares[i].is_block) return true;

    return false;
}

internal bool touchingBlockLeft() {
    for(int i = 0; i < 25; i++) {
        if (shape.squares[i].is_block) {
            int arena_row = (-shape.squares[i].p_y + arena_half_size_y - 2) / 4;
            int arena_col = (shape.squares[i].p_x + arena_half_size_x - 2) / 4;
            
            if (arena[arena_row][arena_col - 1] != arena_color) return true;
        }
    }

    return false;
}

internal bool touchingBlockRight() {
    for(int i = 0; i < 25; i++) {
        if (shape.squares[i].is_block) {
            int arena_row = (-shape.squares[i].p_y + arena_half_size_y - 2) / 4;
            int arena_col = (shape.squares[i].p_x + arena_half_size_x - 2) / 4;
            
            if (arena[arena_row][arena_col + 1] != arena_color) return true;
        }
    }

    return false;
}

internal bool touchingBlockDown() {
    for(int i = 0; i < 25; i++) {
        if (shape.squares[i].is_block) {
            int arena_row = (-shape.squares[i].p_y + arena_half_size_y - 2) / 4;
            int arena_col = (shape.squares[i].p_x + arena_half_size_x - 2) / 4;
            
            if (arena[arena_row + 1][arena_col] != arena_color && shape.squares[i].p_y < arena_half_size_y) return true;
        }
    }

    return false;
}

internal void rotateShape() {
    Shape new_shape;
    for (int i = 0; i < 25; i++) {
        int row = i / 5 - 2;
        int col = i % 5 - 2;

        int new_row = 2 + col;
        int new_col = 2 - row;

        new_shape.squares[new_row * 5 + new_col] = shape.squares[i];
        new_shape.squares[new_row * 5 + new_col].p_x = (shape.squares[i].p_x + (new_col - col) * 4) - 8;
        new_shape.squares[new_row * 5 + new_col].p_y = (shape.squares[i].p_y - (new_row - row) * 4) + 8;
    }

    // Copy elements individually
    for (int i = 0; i < 25; i++) {
        shape.squares[i] = new_shape.squares[i];
    }
}

internal void simulate_game(Input* input, float dt) {
    if (!shape.is_falling) {
        shape.color = colors[getRandomNumber(6)];
        populateSquares();
        shape.is_falling = true;
    }

    time_elapsed += dt;
    fps ++;

    if (time_elapsed >= .4f || (is_down(BUTTON_DOWN) && time_elapsed >= .15f)) {
        std::cout << "FPS: " << fps * 2.5f << '\n';
        fps = 0;
        time_elapsed = 0;

        if (touchingBlockDown()) {
            for(int i = 0; i < 25; i++) {
                if (shape.squares[i].is_block) {
                    int arena_row = (-shape.squares[i].p_y + arena_half_size_y - 2) / 4;
                    int arena_col = (shape.squares[i].p_x + arena_half_size_x - 2) / 4;

                    arena[arena_row][arena_col] = shape.color;
                }
            }

            shape.is_falling = false;
        } else if (!touchingGround()) {
            for(int i = 0; i < 25; i++)
                shape.squares[i].p_y -= 4;
        } else {
            for(int i = 0; i < 25; i++) {
                if (shape.squares[i].is_block) {
                    int arena_row = (-shape.squares[i].p_y + arena_half_size_y - 2) / 4;
                    int arena_col = (shape.squares[i].p_x + arena_half_size_x - 2) / 4;
                    if (arena_row >= 0 && arena_row < arena_y && arena_col >= 0 && arena_col < arena_x) {
                        arena[arena_row][arena_col] = shape.color;
                    }

                    shape.is_falling = false;
                }
            }
        }
    }

    // TODO: Implement check for touching neighbours
    int arena_row = (-shape_p_y + arena_half_size_y - 2) / 4;
    int arena_col = (shape_p_x + arena_half_size_x - 2) / 4;

    if(pressed(BUTTON_LEFT)  && !touchingLeftWall() && !touchingBlockLeft()) {
        for(int i = 0; i < 25; i++)
            shape.squares[i].p_x -= 4;
    }

    if(pressed(BUTTON_RIGHT) && !touchingRightWall() && !touchingBlockRight()) {
        for(int i = 0; i < 25; i++)
            shape.squares[i].p_x += 4;
    }

    if(pressed(BUTTON_UP)) {
        rotateShape();
    }

    // Drawing base
    clean_screen(0x222222);
    
    // Draw arena by matrix state
    for(int y = 0; y < arena_y; y++) {
        for (int x = 0; x < arena_x - 1; x++) {
            float x_draw = -arena_half_size_x + 2 + x * 4;
            float y_draw = arena_half_size_y + 2 - (y + 1) * 4;
            draw_rectangle(x_draw, y_draw, 2.f, 2.f, arena[y][x]);
        }
    }

    // Draw moving shape
    for(int i = 0; i < 25; i++)
        if (shape.squares[i].is_block)
            draw_rectangle(shape.squares[i].p_x, shape.squares[i].p_y, 2.f, 2.f, shape.color);

    // draw_rectangle(38, -48, 2.f, 2.f, 0x0000ff);
    // draw_rectangle(38, -44, 2.f, 2.f, 0x00ff00);
}