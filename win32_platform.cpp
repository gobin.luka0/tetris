// Note for myself: Compile with x86_64-w64-mingw32-g++ win32_platform.cpp -o test.exe -lgdi32 -static-libgcc -static-libstdc++

#include <windows.h>
#include <cstdlib>
#include <iostream>
#include "utils.cpp"

bool running { true };
int fps { 0 };

// Struct that is essentially used for storing screen size and memory required
struct Render_State {
    int height, width;
    void* memory;

    BITMAPINFO bitmapinfo;
};

global_variable Render_State render_state;

#include "platform_common.cpp"
#include "renderer.cpp"
#include "game.cpp"

LRESULT CALLBACK window_callback(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
    LRESULT result = 0;

    switch(uMsg) {
        case WM_CLOSE:
        case WM_DESTROY: {
            running = false;
        } break;

        // Case that gets called if window size gets changed
        case WM_SIZE: {
            RECT rect;

            // Getting information about window
            GetClientRect(hwnd, &rect);

            // Simple calculation for window width and height
            render_state.width = rect.right - rect.left;
            render_state.height = rect.bottom - rect.top;

            // Calculation for the buffer_size (memory needed to display all the pixels)
            int buffer_size = render_state.width * render_state.height * sizeof(u32);

            // Freeing allocated memory if already allocated
            if (render_state.memory) VirtualFree(render_state.memory, 0, MEM_RELEASE);

            // Allocating memory for the necessary number of pixels
            render_state.memory = VirtualAlloc(0, buffer_size, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);

            // Setting up some default render state info
            render_state.bitmapinfo.bmiHeader.biSize = sizeof(render_state.bitmapinfo.bmiHeader);
            render_state.bitmapinfo.bmiHeader.biWidth = render_state.width;
            render_state.bitmapinfo.bmiHeader.biHeight = render_state.height;
            render_state.bitmapinfo.bmiHeader.biPlanes = 1;
            render_state.bitmapinfo.bmiHeader.biBitCount = 32;
            render_state.bitmapinfo.bmiHeader.biCompression = BI_RGB;
        } break;
        default: {
            // DefWindowProc - returning actual window that we need to display
            result = DefWindowProc(hwnd, uMsg, wParam, lParam);
        }
    }

    return result;
}

int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd) {
    // Initialization of game window on Win32
    WNDCLASS window_class = {};
    window_class.style = CS_HREDRAW | CS_VREDRAW;
    window_class.lpfnWndProc = window_callback;
    window_class.hInstance = hInstance;
    window_class.lpszClassName = "Game Window Class";

    // Register window class
    RegisterClass(&window_class);

    // Create window
    HWND win = CreateWindow(window_class.lpszClassName, "Tetris", WS_OVERLAPPEDWINDOW | WS_VISIBLE, CW_USEDEFAULT, CW_USEDEFAULT, 1280, 720, 0, 0, hInstance, 0);
    HDC hdc = GetDC(win);

    Input input = {};

    float delta_time = 0.016666f;

    LARGE_INTEGER frame_begin_time;
    QueryPerformanceCounter(&frame_begin_time);

    float performance_frequency;
    {
        LARGE_INTEGER perf;
        QueryPerformanceFrequency(&perf);
        performance_frequency = (float)perf.QuadPart;
    }

    initializeArena();

    while (running) {
        MSG message;

        // Setting all buttons changed to false for each new frame
        for (int i = 0; i < BUTTON_COUNT; i++) {
            input.buttons[i].changed = false;
        }

        // Simulate
        // Process message
        while (PeekMessage(&message, win, 0, 0, PM_REMOVE)){
            switch(message.message) {
                case WM_KEYUP:
                case WM_KEYDOWN: {
                    u32 vk_code = (u32)message.wParam;

                    bool is_down = ((message.lParam & (1 << 31)) == 0);

// Button processing macro
#define process_button(b, vk)\
case vk: {\
    input.buttons[b].is_down = is_down;\
    input.buttons[b].changed = true;\
} break

                    switch(vk_code){
                        process_button(BUTTON_LEFT, VK_LEFT);
                        process_button(BUTTON_RIGHT, VK_RIGHT);
                        process_button(BUTTON_DOWN, VK_DOWN);
                        process_button(BUTTON_UP, VK_UP);
                    }
                } break;
                default: {
                    TranslateMessage(&message);
                    DispatchMessage(&message);
                }
            }
        }

        simulate_game(&input, delta_time);

        // Render
        StretchDIBits(hdc, 0, 0, render_state.width, render_state.height, 0, 0, render_state.width, render_state.height, render_state.memory, &render_state.bitmapinfo, DIB_RGB_COLORS, SRCCOPY);

        LARGE_INTEGER frame_end_time;
        QueryPerformanceCounter(&frame_end_time);

        delta_time = (float)(frame_end_time.QuadPart - frame_begin_time.QuadPart) / performance_frequency;
        frame_begin_time = frame_end_time;
    }

    return 0;
}