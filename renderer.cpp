// This file contains all of the functions used for rendering different shapes/numbers/text

internal void clean_screen(u32 color) {
    u32* pixel = (u32*)render_state.memory;

    for (int y = 0; y < render_state.height; y++) {
        for (int x = 0; x < render_state.width; x++) {
            *pixel++ = color;
        }
    }
}

internal void draw_rectangle_in_pixels(int x0, int y0, int x1, int y1, u32 color) {
    // Making sure that shape doesn't go outside of the screen which would cause crash
    x0 = clamp(0, x0, render_state.width);
    y0 = clamp(0, y0, render_state.height);
    x1 = clamp(0, x1, render_state.width);
    y1 = clamp(0, y1, render_state.height);

    for (int y = y0; y < y1; y++) {
        u32* pixel = (u32*)render_state.memory + x0 + y * render_state.width;
        for (int x = x0; x < x1; x++) {
            if (x == x0 || x == x1 - 1 || y == y0 || y == y1 - 1) {
                *pixel++ = 0x262626;
            } else *pixel++ = color;
        }
    }
}

// Makes it more convenient to call this render functions
global_variable float render_scale = 0.01f;

// internal int determine_scale() {
//     if(render_state.width >= render_state.height) return render_state.height;

//     return render_state.width;
// }

internal void draw_rectangle(float x, float y, float half_size_x, float half_size_y, u32 color) {
    int scale = render_state.height;

    x *= scale * render_scale;
    y *= scale * render_scale;

    half_size_x *= scale * render_scale;
    half_size_y *= scale * render_scale;

    // Makes the sent x and y center of the rectangle which allows simple drawing calculation
    x += render_state.width / 2;
    y += render_state.height / 2;

    int x0 = x - half_size_x;
    int y0 = y - half_size_y;
    int x1 = x + half_size_x;
    int y1 = y + half_size_y;

    draw_rectangle_in_pixels(x0, y0, x1, y1, color);
}